﻿using PureMagicSpaceStation.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureMagicSpaceStation.Models
{
    public class Booking : Entity
    {
        public virtual Captain Captain { get; set; }
        public virtual Vehicle Vehicle { get; set; }
        public virtual Dock Dock { get; set; }
        public DateTime Time { get; set; } = DateTime.UtcNow;
        public bool Confirmed { get; set; }

    }
}
