﻿using PureMagicSpaceStation.DAL;

namespace PureMagicSpaceStation.Models
{
    public class User : Entity
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public bool IsAdmin { get; set; }

        public string DisplayName
        {
            get
            {
                return UserName + (IsAdmin ? " (Admin)" : "");
            }
        }
    }
}