﻿using PureMagicSpaceStation.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureMagicSpaceStation.Models
{
    public class Vehicle : Entity
    {
        public string ShipName { get; set; }

        public string Cargo { get; set; }

        public string ShipSize { get; set; }
    }
}
