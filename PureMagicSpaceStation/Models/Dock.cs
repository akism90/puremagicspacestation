﻿using PureMagicSpaceStation.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureMagicSpaceStation.Models
{
    public class Dock : Entity
    {
        public int DockNumber { get; set; }
    }
}
