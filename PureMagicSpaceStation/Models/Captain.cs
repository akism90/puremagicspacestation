﻿using PureMagicSpaceStation.DAL;
using System.Collections.Generic;

namespace PureMagicSpaceStation.Models
{
    public class Captain : Entity
    {
        public string Name { get; set; }

        public string Registration { get; set; }

        public string LicenceNo { get; set; }

        public virtual List<Vehicle> Vehicles { get; set; }
    }
}