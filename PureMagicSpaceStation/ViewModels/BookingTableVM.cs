﻿using PureMagicSpaceStation.Models;
using System;

namespace PureMagicSpaceStation.ViewModels
{
    public class BookingTableVM
    {
        public int Id { get; private set; }

        public string Captain { get; private set; }

        public string Time { get; private set; }

        public string Registration { get; private set; }

        public string LicenseNo { get; private set; }

        public string ShipClass { get; private set; }

        public static BookingTableVM FromDomain(Booking dm)
        {
            var vm = new BookingTableVM
            {
                Id = dm.Id,
                Captain = dm.Captain.Name,
                Registration = dm.Captain.Registration,
                LicenseNo = dm.Captain.LicenceNo,
                ShipClass = dm.Vehicle.ShipSize,
                Time = dm.Time.ToString("g")
            };

            return vm;
        }
    }
}