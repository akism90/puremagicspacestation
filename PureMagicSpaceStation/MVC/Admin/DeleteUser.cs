﻿using PureMagicSpaceStation.Models;
using PureMagicSpaceStation.Services;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace PureMagicSpaceStation.MVC.Admin
{
    public partial class DeleteUser : Form
    {   //Declaring singletons and variables to be used
        private AccountService accountService = AccountService.Instance;
        private List<User> users { get; set; }

        public DeleteUser()
        {
            InitializeComponent();

            PopulateUserList();
        }

        //Method to populate the listbox with users
        private void PopulateUserList()
        {
            users = accountService.GetUsers();

            lb_users.DataSource = users;
            lb_users.DisplayMember = "DisplayName";
            lb_users.ValueMember = "Id";
        }
        //When this button is clicked it get the DeleteUser function from the singleton
        private void btn_createUser_Click(object sender, EventArgs e)
        {
            int selectedUserId = Convert.ToInt32(lb_users.SelectedValue);

            accountService.DeleteUser(selectedUserId);

            PopulateUserList();
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            this.Hide();

        }

        private void lb_users_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}