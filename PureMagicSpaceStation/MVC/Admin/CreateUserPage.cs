﻿using PureMagicSpaceStation.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PureMagicSpaceStation.MVC.Admin
{
    public partial class CreateUserPage : Form
    {
        private AccountService accountService = AccountService.Instance;
        public CreateUserPage()
        {
            InitializeComponent();
            lbl_userCreated.Hide();
        }

        private void txt_userName_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_createUser_Click(object sender, EventArgs e)
        {
            var success = accountService.Create(txt_userName.Text, txt_password.Text, chkBx_isAdmin.Checked);

          
            //This is a check to make sure that there is no same username users being created
            if (success == true)
            {
               lbl_userCreated.Text =  $"User {txt_userName.Text}, has been created.";

               lbl_userCreated.Show();

               txt_userName.Clear();
               txt_password.Clear();
               chkBx_isAdmin.Checked = false; 
            }
            else
            {
                lbl_userCreated.Text = "Cannot create user, username already exists ";

                lbl_userCreated.Show();
            }
        }

        private void lbl_userExists_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void lbl_userCreated_Click(object sender, EventArgs e)
        {
           
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            var page = new AdminOptionsPage();
            page.Show();
            this.Hide();
        }
    }
}
