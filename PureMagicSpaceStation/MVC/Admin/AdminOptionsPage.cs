﻿using PureMagicSpaceStation.MVC.Admin;
using PureMagicSpaceStation.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PureMagicSpaceStation
{
    public partial class AdminOptionsPage : Form
    {
        private AccountService accountService = AccountService.Instance;

        public AdminOptionsPage()
        {
            InitializeComponent();
            //string interpolation, gets the username from the account service and puts it in the text in the label
            lbl_welcomeMessage.Text = $"Welcome {accountService.CurrentUser.UserName}. Please select an option below.";
        }

        private void btn_createOperator_Click(object sender, EventArgs e)
        {
            var page = new CreateUserPage();
            page.Show();
            this.Hide();
        }

     

        private void btn_createReport_Click(object sender, EventArgs e)
        {

        }

        private void lbl_welcomeMessage_Click(object sender, EventArgs e)
        {

        }

        private void btn_deleteUser_Click(object sender, EventArgs e)
        {
            var page = new DeleteUser();
            page.Show();
            this.Hide();
        }

        private void btn_logout_Click(object sender, EventArgs e)
        {
            accountService.LogOut();
            var page = new WelcomeLoginPage();
            page.Show();
            this.Hide();
        }

        private void btn_goToOperatorArea_Click(object sender, EventArgs e)
        {
            var page = new OperatorOptionsPage();
            page.Show();
            this.Hide();
        }
    }
}
