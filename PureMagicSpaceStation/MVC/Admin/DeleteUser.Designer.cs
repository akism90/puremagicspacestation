﻿namespace PureMagicSpaceStation.MVC.Admin
{
    partial class DeleteUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_back = new System.Windows.Forms.Button();
            this.btn_createUser = new System.Windows.Forms.Button();
            this.lbl_createOperator = new System.Windows.Forms.Label();
            this.lb_users = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btn_back
            // 
            this.btn_back.Location = new System.Drawing.Point(250, 307);
            this.btn_back.Margin = new System.Windows.Forms.Padding(2);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(99, 41);
            this.btn_back.TabIndex = 15;
            this.btn_back.Text = "Back";
            this.btn_back.UseVisualStyleBackColor = true;
            this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
            // 
            // btn_createUser
            // 
            this.btn_createUser.Location = new System.Drawing.Point(225, 227);
            this.btn_createUser.Margin = new System.Windows.Forms.Padding(2);
            this.btn_createUser.Name = "btn_createUser";
            this.btn_createUser.Size = new System.Drawing.Size(124, 41);
            this.btn_createUser.TabIndex = 14;
            this.btn_createUser.Text = "Delete User";
            this.btn_createUser.UseVisualStyleBackColor = true;
            this.btn_createUser.Click += new System.EventHandler(this.btn_createUser_Click);
            // 
            // lbl_createOperator
            // 
            this.lbl_createOperator.AutoSize = true;
            this.lbl_createOperator.Location = new System.Drawing.Point(11, 9);
            this.lbl_createOperator.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_createOperator.Name = "lbl_createOperator";
            this.lbl_createOperator.Size = new System.Drawing.Size(63, 13);
            this.lbl_createOperator.TabIndex = 8;
            this.lbl_createOperator.Text = "Delete User";
            // 
            // lb_users
            // 
            this.lb_users.FormattingEnabled = true;
            this.lb_users.Location = new System.Drawing.Point(12, 38);
            this.lb_users.Name = "lb_users";
            this.lb_users.Size = new System.Drawing.Size(337, 173);
            this.lb_users.TabIndex = 16;
            this.lb_users.SelectedIndexChanged += new System.EventHandler(this.lb_users_SelectedIndexChanged);
            // 
            // DeleteUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 372);
            this.Controls.Add(this.lb_users);
            this.Controls.Add(this.btn_back);
            this.Controls.Add(this.btn_createUser);
            this.Controls.Add(this.lbl_createOperator);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "DeleteUser";
            this.Text = "DeleteUser";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_back;
        private System.Windows.Forms.Button btn_createUser;
        private System.Windows.Forms.Label lbl_createOperator;
        private System.Windows.Forms.ListBox lb_users;
    }
}