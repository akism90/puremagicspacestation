﻿namespace PureMagicSpaceStation
{
    partial class AdminOptionsPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_chooseAnOption = new System.Windows.Forms.Label();
            this.btn_createUser = new System.Windows.Forms.Button();
            this.btn_createReport = new System.Windows.Forms.Button();
            this.btn_deleteUser = new System.Windows.Forms.Button();
            this.btn_logout = new System.Windows.Forms.Button();
            this.btn_goToOperatorArea = new System.Windows.Forms.Button();
            this.lbl_welcomeMessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_chooseAnOption
            // 
            this.lbl_chooseAnOption.AutoSize = true;
            this.lbl_chooseAnOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chooseAnOption.Location = new System.Drawing.Point(185, 52);
            this.lbl_chooseAnOption.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_chooseAnOption.Name = "lbl_chooseAnOption";
            this.lbl_chooseAnOption.Size = new System.Drawing.Size(232, 31);
            this.lbl_chooseAnOption.TabIndex = 0;
            this.lbl_chooseAnOption.Text = "Choose an Option";
            // 
            // btn_createUser
            // 
            this.btn_createUser.Location = new System.Drawing.Point(169, 242);
            this.btn_createUser.Margin = new System.Windows.Forms.Padding(2);
            this.btn_createUser.Name = "btn_createUser";
            this.btn_createUser.Size = new System.Drawing.Size(134, 34);
            this.btn_createUser.TabIndex = 1;
            this.btn_createUser.Text = "Create User";
            this.btn_createUser.UseVisualStyleBackColor = true;
            this.btn_createUser.Click += new System.EventHandler(this.btn_createOperator_Click);
            // 
            // btn_createReport
            // 
            this.btn_createReport.Location = new System.Drawing.Point(317, 242);
            this.btn_createReport.Margin = new System.Windows.Forms.Padding(2);
            this.btn_createReport.Name = "btn_createReport";
            this.btn_createReport.Size = new System.Drawing.Size(134, 34);
            this.btn_createReport.TabIndex = 3;
            this.btn_createReport.Text = "Create Report";
            this.btn_createReport.UseVisualStyleBackColor = true;
            this.btn_createReport.Click += new System.EventHandler(this.btn_createReport_Click);
            // 
            // btn_deleteUser
            // 
            this.btn_deleteUser.Location = new System.Drawing.Point(169, 289);
            this.btn_deleteUser.Margin = new System.Windows.Forms.Padding(2);
            this.btn_deleteUser.Name = "btn_deleteUser";
            this.btn_deleteUser.Size = new System.Drawing.Size(134, 34);
            this.btn_deleteUser.TabIndex = 4;
            this.btn_deleteUser.Text = "Delete User";
            this.btn_deleteUser.UseVisualStyleBackColor = true;
            this.btn_deleteUser.Click += new System.EventHandler(this.btn_deleteUser_Click);
            // 
            // btn_logout
            // 
            this.btn_logout.Location = new System.Drawing.Point(407, 439);
            this.btn_logout.Margin = new System.Windows.Forms.Padding(2);
            this.btn_logout.Name = "btn_logout";
            this.btn_logout.Size = new System.Drawing.Size(134, 34);
            this.btn_logout.TabIndex = 6;
            this.btn_logout.Text = "Logout";
            this.btn_logout.UseVisualStyleBackColor = true;
            this.btn_logout.Click += new System.EventHandler(this.btn_logout_Click);
            // 
            // btn_goToOperatorArea
            // 
            this.btn_goToOperatorArea.Location = new System.Drawing.Point(317, 289);
            this.btn_goToOperatorArea.Margin = new System.Windows.Forms.Padding(2);
            this.btn_goToOperatorArea.Name = "btn_goToOperatorArea";
            this.btn_goToOperatorArea.Size = new System.Drawing.Size(134, 34);
            this.btn_goToOperatorArea.TabIndex = 7;
            this.btn_goToOperatorArea.Text = "Go To Operator Area";
            this.btn_goToOperatorArea.UseVisualStyleBackColor = true;
            this.btn_goToOperatorArea.Click += new System.EventHandler(this.btn_goToOperatorArea_Click);
            // 
            // lbl_welcomeMessage
            // 
            this.lbl_welcomeMessage.AutoSize = true;
            this.lbl_welcomeMessage.Location = new System.Drawing.Point(169, 112);
            this.lbl_welcomeMessage.Name = "lbl_welcomeMessage";
            this.lbl_welcomeMessage.Size = new System.Drawing.Size(98, 13);
            this.lbl_welcomeMessage.TabIndex = 8;
            this.lbl_welcomeMessage.Text = "Welcome Message";
            this.lbl_welcomeMessage.Click += new System.EventHandler(this.lbl_welcomeMessage_Click);
            // 
            // AdminOptionsPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 574);
            this.Controls.Add(this.lbl_welcomeMessage);
            this.Controls.Add(this.btn_goToOperatorArea);
            this.Controls.Add(this.btn_logout);
            this.Controls.Add(this.btn_deleteUser);
            this.Controls.Add(this.btn_createReport);
            this.Controls.Add(this.btn_createUser);
            this.Controls.Add(this.lbl_chooseAnOption);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "AdminOptionsPage";
            this.Text = "AdminOptionsPage";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_chooseAnOption;
        private System.Windows.Forms.Button btn_createUser;
        private System.Windows.Forms.Button btn_createReport;
        private System.Windows.Forms.Button btn_deleteUser;
        private System.Windows.Forms.Button btn_logout;
        private System.Windows.Forms.Button btn_goToOperatorArea;
        private System.Windows.Forms.Label lbl_welcomeMessage;
    }
}