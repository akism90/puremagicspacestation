﻿namespace PureMagicSpaceStation.MVC.Admin
{
    partial class CreateUserPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_createOperator = new System.Windows.Forms.Label();
            this.lbl_userName = new System.Windows.Forms.Label();
            this.lbl_Password = new System.Windows.Forms.Label();
            this.chkBx_isAdmin = new System.Windows.Forms.CheckBox();
            this.txt_userName = new System.Windows.Forms.TextBox();
            this.txt_password = new System.Windows.Forms.TextBox();
            this.btn_createUser = new System.Windows.Forms.Button();
            this.btn_back = new System.Windows.Forms.Button();
            this.lbl_userCreated = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_createOperator
            // 
            this.lbl_createOperator.AutoSize = true;
            this.lbl_createOperator.Location = new System.Drawing.Point(213, 29);
            this.lbl_createOperator.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_createOperator.Name = "lbl_createOperator";
            this.lbl_createOperator.Size = new System.Drawing.Size(82, 13);
            this.lbl_createOperator.TabIndex = 0;
            this.lbl_createOperator.Text = "Create Operator";
            // 
            // lbl_userName
            // 
            this.lbl_userName.AutoSize = true;
            this.lbl_userName.Location = new System.Drawing.Point(180, 110);
            this.lbl_userName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_userName.Name = "lbl_userName";
            this.lbl_userName.Size = new System.Drawing.Size(58, 13);
            this.lbl_userName.TabIndex = 1;
            this.lbl_userName.Text = "Username:";
            // 
            // lbl_Password
            // 
            this.lbl_Password.AutoSize = true;
            this.lbl_Password.Location = new System.Drawing.Point(180, 149);
            this.lbl_Password.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_Password.Name = "lbl_Password";
            this.lbl_Password.Size = new System.Drawing.Size(56, 13);
            this.lbl_Password.TabIndex = 2;
            this.lbl_Password.Text = "Password:";
            // 
            // chkBx_isAdmin
            // 
            this.chkBx_isAdmin.AutoSize = true;
            this.chkBx_isAdmin.Location = new System.Drawing.Point(183, 199);
            this.chkBx_isAdmin.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.chkBx_isAdmin.Name = "chkBx_isAdmin";
            this.chkBx_isAdmin.Size = new System.Drawing.Size(85, 17);
            this.chkBx_isAdmin.TabIndex = 3;
            this.chkBx_isAdmin.Text = "Make Admin";
            this.chkBx_isAdmin.UseVisualStyleBackColor = true;
            // 
            // txt_userName
            // 
            this.txt_userName.Location = new System.Drawing.Point(267, 110);
            this.txt_userName.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txt_userName.Name = "txt_userName";
            this.txt_userName.Size = new System.Drawing.Size(138, 20);
            this.txt_userName.TabIndex = 4;
            this.txt_userName.TextChanged += new System.EventHandler(this.txt_userName_TextChanged);
            // 
            // txt_password
            // 
            this.txt_password.Location = new System.Drawing.Point(267, 144);
            this.txt_password.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txt_password.Name = "txt_password";
            this.txt_password.Size = new System.Drawing.Size(138, 20);
            this.txt_password.TabIndex = 5;
            // 
            // btn_createUser
            // 
            this.btn_createUser.Location = new System.Drawing.Point(176, 322);
            this.btn_createUser.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_createUser.Name = "btn_createUser";
            this.btn_createUser.Size = new System.Drawing.Size(99, 41);
            this.btn_createUser.TabIndex = 6;
            this.btn_createUser.Text = "Create User";
            this.btn_createUser.UseVisualStyleBackColor = true;
            this.btn_createUser.Click += new System.EventHandler(this.btn_createUser_Click);
            // 
            // btn_back
            // 
            this.btn_back.Location = new System.Drawing.Point(428, 497);
            this.btn_back.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(99, 41);
            this.btn_back.TabIndex = 7;
            this.btn_back.Text = "Back";
            this.btn_back.UseVisualStyleBackColor = true;
            this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
            // 
            // lbl_userCreated
            // 
            this.lbl_userCreated.AutoSize = true;
            this.lbl_userCreated.Location = new System.Drawing.Point(183, 259);
            this.lbl_userCreated.Name = "lbl_userCreated";
            this.lbl_userCreated.Size = new System.Drawing.Size(66, 13);
            this.lbl_userCreated.TabIndex = 9;
            this.lbl_userCreated.Text = "user created";
            this.lbl_userCreated.Click += new System.EventHandler(this.lbl_userCreated_Click);
            // 
            // CreateUserPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 574);
            this.Controls.Add(this.lbl_userCreated);
            this.Controls.Add(this.btn_back);
            this.Controls.Add(this.btn_createUser);
            this.Controls.Add(this.txt_password);
            this.Controls.Add(this.txt_userName);
            this.Controls.Add(this.chkBx_isAdmin);
            this.Controls.Add(this.lbl_Password);
            this.Controls.Add(this.lbl_userName);
            this.Controls.Add(this.lbl_createOperator);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "CreateUserPage";
            this.Text = "AddOperatorPage";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_createOperator;
        private System.Windows.Forms.Label lbl_userName;
        private System.Windows.Forms.Label lbl_Password;
        private System.Windows.Forms.CheckBox chkBx_isAdmin;
        private System.Windows.Forms.TextBox txt_userName;
        private System.Windows.Forms.TextBox txt_password;
        private System.Windows.Forms.Button btn_createUser;
        private System.Windows.Forms.Button btn_back;
        private System.Windows.Forms.Label lbl_userCreated;
    }
}