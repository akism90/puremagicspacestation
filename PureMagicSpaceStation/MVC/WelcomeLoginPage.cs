﻿using PureMagicSpaceStation.Services;
using System;
using System.Windows.Forms;

namespace PureMagicSpaceStation
{
    public partial class WelcomeLoginPage : Form
    {
        private AccountService AccountService = AccountService.Instance;

        public WelcomeLoginPage()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void label3_Click(object sender, EventArgs e)
        {
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            var user = AccountService.LogIn(txt_username.Text, txt_password.Text);
            if (user != null)
            {
                // Logged in
                lbl_Warning.Hide();

                if (user.IsAdmin)
                {
                    new AdminOptionsPage().Show();
                }
                else
                {
                    var page = new OperatorOptionsPage();
                    page.Show();
                }

                // Kill this screen
                this.Hide();
            }
            else
            {
                // Show an error
                Console.Beep();
                lbl_Warning.Show();
            }
        }

        private void txt_username_TextChanged(object sender, EventArgs e)
        {
        }

        private void label1_Click_1(object sender, EventArgs e)
        {
        }

        private void WelcomeLoginPage_Load(object sender, EventArgs e)
        {

        }
    }
}