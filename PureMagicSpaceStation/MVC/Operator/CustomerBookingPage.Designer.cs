﻿namespace PureMagicSpaceStation.MVC
{
    partial class CustomerBookingPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_book = new System.Windows.Forms.Button();
            this.btn_back = new System.Windows.Forms.Button();
            this.dgBookings = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgBookings)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_book
            // 
            this.btn_book.Location = new System.Drawing.Point(461, 369);
            this.btn_book.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_book.Name = "btn_book";
            this.btn_book.Size = new System.Drawing.Size(117, 25);
            this.btn_book.TabIndex = 3;
            this.btn_book.Text = "New Booking";
            this.btn_book.UseVisualStyleBackColor = true;
            this.btn_book.Click += new System.EventHandler(this.btn_book_Click);
            // 
            // btn_back
            // 
            this.btn_back.Location = new System.Drawing.Point(461, 401);
            this.btn_back.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(117, 25);
            this.btn_back.TabIndex = 4;
            this.btn_back.Text = "Back";
            this.btn_back.UseVisualStyleBackColor = true;
            this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
            // 
            // dgBookings
            // 
            this.dgBookings.AllowUserToAddRows = false;
            this.dgBookings.AllowUserToDeleteRows = false;
            this.dgBookings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgBookings.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgBookings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBookings.Location = new System.Drawing.Point(12, 56);
            this.dgBookings.Name = "dgBookings";
            this.dgBookings.ReadOnly = true;
            this.dgBookings.Size = new System.Drawing.Size(563, 262);
            this.dgBookings.TabIndex = 5;
            this.dgBookings.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // CustomerBookingPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(587, 437);
            this.Controls.Add(this.dgBookings);
            this.Controls.Add(this.btn_back);
            this.Controls.Add(this.btn_book);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "CustomerBookingPage";
            this.Text = "CustomerBookingPage";
            ((System.ComponentModel.ISupportInitialize)(this.dgBookings)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn_book;
        private System.Windows.Forms.Button btn_back;
        private System.Windows.Forms.DataGridView dgBookings;
    }
}