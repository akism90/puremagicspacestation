﻿using PureMagicSpaceStation.Services;
using PureMagicSpaceStation.ViewModels;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace PureMagicSpaceStation.MVC
{
    public partial class CustomerBookingPage : Form
    {
        private BookingService bookingService = BookingService.Instance;
        private AccountService accountService = AccountService.Instance;

        public CustomerBookingPage()
        {
            InitializeComponent();
            PopulateDataGrid();
        }

        public void PopulateDataGrid()
        {
            // We need to convert our domain models to view models using a .Select
            // Our view models have strings for the Captains name as we can't show this
            // using the domain model directly because it's with a child object.
            var bookings = bookingService.GetBookings()
                .OrderByDescending(booking => booking.Time)
                .Select(booking => BookingTableVM.FromDomain(booking))
                .ToList();

            dgBookings.AutoGenerateColumns = false;
            dgBookings.DataSource = bookings;

            dgBookings.Columns.Add(new DataGridViewTextBoxColumn
            {
                Name = "Captain",
                HeaderText = "Captain",
                DataPropertyName = "Captain"
            });
            dgBookings.Columns.Add(new DataGridViewTextBoxColumn
            {
                Name = "Registration",
                HeaderText = "Registration",
                DataPropertyName = "Registration"
            });
            dgBookings.Columns.Add(new DataGridViewTextBoxColumn
            {
                Name = "LicenseNo",
                HeaderText = "License Number",
                DataPropertyName = "LicenseNo"
            });
            dgBookings.Columns.Add(new DataGridViewTextBoxColumn
            {
                Name = "ShipClass",
                HeaderText = "Ship Class",
                DataPropertyName = "ShipClass"
            });
            dgBookings.Columns.Add(new DataGridViewTextBoxColumn
            {
                Name = "Time",
                HeaderText = "Time",
                DataPropertyName = "Time"
            });
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            this.Hide();
            new OperatorOptionsPage();
        }

        private void btn_book_Click(object sender, EventArgs e)
        {
            this.Hide();
            new CancelCustomerBookingPage().Show();
        }
    }
}