﻿using PureMagicSpaceStation.MVC;
using PureMagicSpaceStation.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PureMagicSpaceStation
{
    public partial class OperatorOptionsPage : Form
    {
        private AccountService accountService = AccountService.Instance;

        public OperatorOptionsPage()
        {
            InitializeComponent();

            lbl_welcomeMessage.Text = $"Welcome {accountService.CurrentUser.UserName}. Please select an option below.";
        }

        private void btn_bookCustomer_Click(object sender, EventArgs e)
        {
            var page = new CustomerBookingPage();
            page.Show();
            this.Hide();
        }

        private void btn_cancelBooking_Click(object sender, EventArgs e)
        {
            var page = new CancelCustomerBookingPage();
            page.Show();
            this.Hide();
        }

        private void btn_logout_Click(object sender, EventArgs e)
        {
            accountService.LogOut();
            var page = new WelcomeLoginPage();
            page.Show();
            this.Hide();
        }

        private void OperatorOptionsPage_Load(object sender, EventArgs e)
        {

        }

        private void lbl_welcomeMessage_Click(object sender, EventArgs e)
        {

        }
    }
}
