﻿namespace PureMagicSpaceStation.MVC
{
    partial class CancelCustomerBookingPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.btn_cancelBooking = new System.Windows.Forms.Button();
            this.btn_back = new System.Windows.Forms.Button();
            this.lb_captains = new System.Windows.Forms.ListBox();
            this.lb_ships = new System.Windows.Forms.ListBox();
            this.lb_times = new System.Windows.Forms.ListBox();
            this.btnBook = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(48, 24);
            this.dtpDate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(135, 20);
            this.dtpDate.TabIndex = 0;
            // 
            // btn_cancelBooking
            // 
            this.btn_cancelBooking.Location = new System.Drawing.Point(413, 467);
            this.btn_cancelBooking.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_cancelBooking.Name = "btn_cancelBooking";
            this.btn_cancelBooking.Size = new System.Drawing.Size(103, 23);
            this.btn_cancelBooking.TabIndex = 2;
            this.btn_cancelBooking.Text = "Cancel Booking";
            this.btn_cancelBooking.UseVisualStyleBackColor = true;
            this.btn_cancelBooking.Click += new System.EventHandler(this.btn_cancelBooking_Click);
            // 
            // btn_back
            // 
            this.btn_back.Location = new System.Drawing.Point(453, 519);
            this.btn_back.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(63, 23);
            this.btn_back.TabIndex = 3;
            this.btn_back.Text = "Back";
            this.btn_back.UseVisualStyleBackColor = true;
            // 
            // lb_captains
            // 
            this.lb_captains.FormattingEnabled = true;
            this.lb_captains.Location = new System.Drawing.Point(11, 95);
            this.lb_captains.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lb_captains.Name = "lb_captains";
            this.lb_captains.Size = new System.Drawing.Size(172, 238);
            this.lb_captains.TabIndex = 4;
            this.lb_captains.SelectedIndexChanged += new System.EventHandler(this.lb_captains_SelectedIndexChanged);
            // 
            // lb_ships
            // 
            this.lb_ships.FormattingEnabled = true;
            this.lb_ships.Location = new System.Drawing.Point(228, 95);
            this.lb_ships.Margin = new System.Windows.Forms.Padding(2);
            this.lb_ships.Name = "lb_ships";
            this.lb_ships.Size = new System.Drawing.Size(151, 238);
            this.lb_ships.TabIndex = 5;
            // 
            // lb_times
            // 
            this.lb_times.FormattingEnabled = true;
            this.lb_times.Location = new System.Drawing.Point(433, 95);
            this.lb_times.Margin = new System.Windows.Forms.Padding(2);
            this.lb_times.Name = "lb_times";
            this.lb_times.Size = new System.Drawing.Size(151, 238);
            this.lb_times.TabIndex = 6;
            // 
            // btnBook
            // 
            this.btnBook.Location = new System.Drawing.Point(481, 359);
            this.btnBook.Margin = new System.Windows.Forms.Padding(2);
            this.btnBook.Name = "btnBook";
            this.btnBook.Size = new System.Drawing.Size(103, 23);
            this.btnBook.TabIndex = 7;
            this.btnBook.Text = "Create Booking";
            this.btnBook.UseVisualStyleBackColor = true;
            this.btnBook.Click += new System.EventHandler(this.btnBook_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Captain";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(225, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Ship";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(430, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Hour Slot";
            // 
            // CancelCustomerBookingPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 574);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnBook);
            this.Controls.Add(this.lb_times);
            this.Controls.Add(this.lb_ships);
            this.Controls.Add(this.lb_captains);
            this.Controls.Add(this.btn_back);
            this.Controls.Add(this.btn_cancelBooking);
            this.Controls.Add(this.dtpDate);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "CancelCustomerBookingPage";
            this.Text = "CancelCustomerBookingPage";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Button btn_cancelBooking;
        private System.Windows.Forms.Button btn_back;
        private System.Windows.Forms.ListBox lb_captains;
        private System.Windows.Forms.ListBox lb_ships;
        private System.Windows.Forms.ListBox lb_times;
        private System.Windows.Forms.Button btnBook;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}