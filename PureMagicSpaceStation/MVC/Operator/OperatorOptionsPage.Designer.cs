﻿namespace PureMagicSpaceStation
{
    partial class OperatorOptionsPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_chooseAnOption = new System.Windows.Forms.Label();
            this.btn_bookCustomer = new System.Windows.Forms.Button();
            this.btn_cancelBooking = new System.Windows.Forms.Button();
            this.btn_logout = new System.Windows.Forms.Button();
            this.lbl_welcomeMessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_chooseAnOption
            // 
            this.lbl_chooseAnOption.AutoSize = true;
            this.lbl_chooseAnOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chooseAnOption.Location = new System.Drawing.Point(278, 80);
            this.lbl_chooseAnOption.Name = "lbl_chooseAnOption";
            this.lbl_chooseAnOption.Size = new System.Drawing.Size(359, 46);
            this.lbl_chooseAnOption.TabIndex = 0;
            this.lbl_chooseAnOption.Text = "Choose an Option";
            // 
            // btn_bookCustomer
            // 
            this.btn_bookCustomer.Location = new System.Drawing.Point(346, 255);
            this.btn_bookCustomer.Name = "btn_bookCustomer";
            this.btn_bookCustomer.Size = new System.Drawing.Size(201, 52);
            this.btn_bookCustomer.TabIndex = 1;
            this.btn_bookCustomer.Text = "Book Customer";
            this.btn_bookCustomer.UseVisualStyleBackColor = true;
            this.btn_bookCustomer.Click += new System.EventHandler(this.btn_bookCustomer_Click);
            // 
            // btn_cancelBooking
            // 
            this.btn_cancelBooking.Location = new System.Drawing.Point(346, 372);
            this.btn_cancelBooking.Name = "btn_cancelBooking";
            this.btn_cancelBooking.Size = new System.Drawing.Size(201, 52);
            this.btn_cancelBooking.TabIndex = 2;
            this.btn_cancelBooking.Text = "Cancel Booking";
            this.btn_cancelBooking.UseVisualStyleBackColor = true;
            this.btn_cancelBooking.Click += new System.EventHandler(this.btn_cancelBooking_Click);
            // 
            // btn_logout
            // 
            this.btn_logout.Location = new System.Drawing.Point(346, 486);
            this.btn_logout.Name = "btn_logout";
            this.btn_logout.Size = new System.Drawing.Size(201, 52);
            this.btn_logout.TabIndex = 3;
            this.btn_logout.Text = "Logout";
            this.btn_logout.UseVisualStyleBackColor = true;
            this.btn_logout.Click += new System.EventHandler(this.btn_logout_Click);
            // 
            // lbl_welcomeMessage
            // 
            this.lbl_welcomeMessage.AutoSize = true;
            this.lbl_welcomeMessage.Location = new System.Drawing.Point(315, 172);
            this.lbl_welcomeMessage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_welcomeMessage.Name = "lbl_welcomeMessage";
            this.lbl_welcomeMessage.Size = new System.Drawing.Size(113, 20);
            this.lbl_welcomeMessage.TabIndex = 4;
            this.lbl_welcomeMessage.Text = "Welcome User";
            this.lbl_welcomeMessage.Click += new System.EventHandler(this.lbl_welcomeMessage_Click);
            // 
            // OperatorOptionsPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(892, 883);
            this.Controls.Add(this.lbl_welcomeMessage);
            this.Controls.Add(this.btn_logout);
            this.Controls.Add(this.btn_cancelBooking);
            this.Controls.Add(this.btn_bookCustomer);
            this.Controls.Add(this.lbl_chooseAnOption);
            this.Name = "OperatorOptionsPage";
            this.Text = "OperatorOptionsPage";
            this.Load += new System.EventHandler(this.OperatorOptionsPage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_chooseAnOption;
        private System.Windows.Forms.Button btn_bookCustomer;
        private System.Windows.Forms.Button btn_cancelBooking;
        private System.Windows.Forms.Button btn_logout;
        private System.Windows.Forms.Label lbl_welcomeMessage;
    }
}