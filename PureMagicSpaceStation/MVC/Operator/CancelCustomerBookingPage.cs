﻿using PureMagicSpaceStation.Models;
using PureMagicSpaceStation.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PureMagicSpaceStation.MVC
{
    public partial class CancelCustomerBookingPage : Form
    {
        private BookingService bookingService = BookingService.Instance;
        private AccountService accountService = AccountService.Instance;
        private CaptainService captainService = CaptainService.Instance;
        private VehicleService vehicleService = VehicleService.Instance;

        public CancelCustomerBookingPage()
        {
            InitializeComponent();
            PopulateCaptainsListBox();
            PopulateTimeListBox();
        }

        public void PopulateTimeListBox()
        {
            var times = new List<int>();

            for(var i = 0; i < 24; i++)
            {
                times.Add(i);
            }

            lb_times.DataSource = times;
        }

        private void PopulateCaptainsListBox()
        {
            var captains = captainService.GetCaptains();

            lb_captains.DataSource = captains;
            lb_captains.DisplayMember = "Name";
            lb_captains.ValueMember = "Id";
        }

        private void PopulateVehicleListBox(int captainId)
        {
            var vehicles = vehicleService.GetCaptainsVehicles(captainId);

            lb_ships.DataSource = vehicles;
            lb_ships.DisplayMember = "ShipName";
            lb_ships.ValueMember = "Id";
        }

        private void btn_cancelBooking_Click(object sender, EventArgs e)
        {

        }

        private void lb_captains_SelectedIndexChanged(object sender, EventArgs e)
        {
            // We could pass in SelectedItem directly as it is the Captain and has a reference
            // to his vehicles. Later on however we may make this a VM and it won't contain the vehicles.
            PopulateVehicleListBox((lb_captains.SelectedItem as Captain).Id);
        }

        private void btnBook_Click(object sender, EventArgs e)
        {
            var bookingTime = new DateTime(dtpDate.Value.Year, dtpDate.Value.Month, dtpDate.Value.Day,
                Convert.ToInt32(lb_times.SelectedItem), 0, 0);

            var captainId = (lb_captains.SelectedItem as Captain).Id;
            var vehicleId = (lb_ships.SelectedItem as Vehicle).Id;

            if(bookingService.CreateBooking(captainId, vehicleId, bookingTime))
            {
                this.Hide();
                new CustomerBookingPage().Show();
            } else
            {
                // Show error message (already booked in at that time)
            }
        }
    }
}
