﻿namespace PureMagicSpaceStation
{
    partial class AdminOptionsPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_chooseAnOption = new System.Windows.Forms.Label();
            this.btn_createOperator = new System.Windows.Forms.Button();
            this.btn_createAdmin = new System.Windows.Forms.Button();
            this.btn_createReport = new System.Windows.Forms.Button();
            this.btn_deleteOperator = new System.Windows.Forms.Button();
            this.btn_deleteAdmin = new System.Windows.Forms.Button();
            this.btn_logout = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_chooseAnOption
            // 
            this.lbl_chooseAnOption.AutoSize = true;
            this.lbl_chooseAnOption.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_chooseAnOption.Location = new System.Drawing.Point(278, 80);
            this.lbl_chooseAnOption.Name = "lbl_chooseAnOption";
            this.lbl_chooseAnOption.Size = new System.Drawing.Size(343, 46);
            this.lbl_chooseAnOption.TabIndex = 0;
            this.lbl_chooseAnOption.Text = "Choose an Option";
            // 
            // btn_createOperator
            // 
            this.btn_createOperator.Location = new System.Drawing.Point(119, 373);
            this.btn_createOperator.Name = "btn_createOperator";
            this.btn_createOperator.Size = new System.Drawing.Size(201, 52);
            this.btn_createOperator.TabIndex = 1;
            this.btn_createOperator.Text = "Create Operator";
            this.btn_createOperator.UseVisualStyleBackColor = true;
            this.btn_createOperator.Click += new System.EventHandler(this.btn_createOperator_Click);
            // 
            // btn_createAdmin
            // 
            this.btn_createAdmin.Location = new System.Drawing.Point(354, 373);
            this.btn_createAdmin.Name = "btn_createAdmin";
            this.btn_createAdmin.Size = new System.Drawing.Size(201, 52);
            this.btn_createAdmin.TabIndex = 2;
            this.btn_createAdmin.Text = "Create Admin";
            this.btn_createAdmin.UseVisualStyleBackColor = true;
            this.btn_createAdmin.Click += new System.EventHandler(this.btn_createAdmin_Click);
            // 
            // btn_createReport
            // 
            this.btn_createReport.Location = new System.Drawing.Point(580, 373);
            this.btn_createReport.Name = "btn_createReport";
            this.btn_createReport.Size = new System.Drawing.Size(201, 52);
            this.btn_createReport.TabIndex = 3;
            this.btn_createReport.Text = "Create Report";
            this.btn_createReport.UseVisualStyleBackColor = true;
            this.btn_createReport.Click += new System.EventHandler(this.btn_createReport_Click);
            // 
            // btn_deleteOperator
            // 
            this.btn_deleteOperator.Location = new System.Drawing.Point(119, 444);
            this.btn_deleteOperator.Name = "btn_deleteOperator";
            this.btn_deleteOperator.Size = new System.Drawing.Size(201, 52);
            this.btn_deleteOperator.TabIndex = 4;
            this.btn_deleteOperator.Text = "Delete Operator";
            this.btn_deleteOperator.UseVisualStyleBackColor = true;
            // 
            // btn_deleteAdmin
            // 
            this.btn_deleteAdmin.Location = new System.Drawing.Point(354, 444);
            this.btn_deleteAdmin.Name = "btn_deleteAdmin";
            this.btn_deleteAdmin.Size = new System.Drawing.Size(201, 52);
            this.btn_deleteAdmin.TabIndex = 5;
            this.btn_deleteAdmin.Text = "Delete Admin";
            this.btn_deleteAdmin.UseVisualStyleBackColor = true;
            // 
            // btn_logout
            // 
            this.btn_logout.Location = new System.Drawing.Point(610, 675);
            this.btn_logout.Name = "btn_logout";
            this.btn_logout.Size = new System.Drawing.Size(201, 52);
            this.btn_logout.TabIndex = 6;
            this.btn_logout.Text = "Logout";
            this.btn_logout.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(580, 444);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(201, 52);
            this.button1.TabIndex = 7;
            this.button1.Text = "Go To Operator Area";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // AdminOptionsPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(893, 883);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_logout);
            this.Controls.Add(this.btn_deleteAdmin);
            this.Controls.Add(this.btn_deleteOperator);
            this.Controls.Add(this.btn_createReport);
            this.Controls.Add(this.btn_createAdmin);
            this.Controls.Add(this.btn_createOperator);
            this.Controls.Add(this.lbl_chooseAnOption);
            this.Name = "AdminOptionsPage";
            this.Text = "AdminOptionsPage";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_chooseAnOption;
        private System.Windows.Forms.Button btn_createOperator;
        private System.Windows.Forms.Button btn_createAdmin;
        private System.Windows.Forms.Button btn_createReport;
        private System.Windows.Forms.Button btn_deleteOperator;
        private System.Windows.Forms.Button btn_deleteAdmin;
        private System.Windows.Forms.Button btn_logout;
        private System.Windows.Forms.Button button1;
    }
}