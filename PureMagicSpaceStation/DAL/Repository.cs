﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PureMagicSpaceStation.DAL
{
    // Generic repository pattern for accessing data for stored entities
    public class Repository<T> where T : Entity
    {
        // Stores our data objects
        static private List<T> data = new List<T>();

        // Our single instance of the repository
        static private Repository<T> _instance;

        // We create the repository as a singleton so we are always
        // working with the same data.
        public static Repository<T> Instance
        {
            get
            {
                // On first request of the repository it won't exist
                if (_instance == null)
                    // So we create one
                    _instance = new Repository<T>();

                // Every other time, return the one we created earlier.
                return _instance;
            }
        }

        public Repository()
        {
            data = new List<T>();
        }

        public T Find(int id)
        {
            return data.SingleOrDefault(i => i.Id == id);
        }

        public T Find(Func<T, bool> predicate)
        {
            return data.SingleOrDefault(predicate);
        }

        public void Delete(int userId)
        {
            var user = Find(userId);
            if (user == null)
                return;

            user.IsDeleted = true;
        }

        public void Add(T item)
        {
            data.Add(item);
        }

        public List <T> GetAll()
        {
            return data;
        }

        public void Delete(T item)
        {
            //data.Remove(item);
            item.IsDeleted = true;
        }

        public void Commit()
        {
        }
    }
}