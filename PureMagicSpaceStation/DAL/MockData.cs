﻿using PureMagicSpaceStation.Models;
using System;
using System.Collections.Generic;

namespace PureMagicSpaceStation.DAL
{
    public class MockData
    {
        private static Repository<User> _userRepo = Repository<User>.Instance;
        private static Repository<Captain> _captainRepo = Repository<Captain>.Instance;
        private static Repository<Vehicle> _vehicleRepo = Repository<Vehicle>.Instance;
        private static Repository<Dock> _dockRepo = Repository<Dock>.Instance;
        private static Repository<Booking> _BookingRepo = Repository<Booking>.Instance;


        // This method simply intialises some test data into our repositories.
        static public void Seed()
        {
            // Create a user for us to log in as
            _userRepo.Add(new User { Id = 0, UserName = "Andy", Password = "password" });
            _userRepo.Add(new User { Id = 1, UserName = "Andrew", Password = "password", IsAdmin = true });
            _userRepo.Add(new User { Id = 2, UserName = "a", Password = "", IsAdmin = true });

            var v1 = new Vehicle { Id = 0, ShipSize = "1", Cargo = "sausages", ShipName = "The Great Ship" };
            _vehicleRepo.Add(v1);
            var v2 = new Vehicle { Id = 1, ShipSize = "2", Cargo = " more sausages", ShipName = "Not Quite As Great" };
            _vehicleRepo.Add(v2);
            var v3 = new Vehicle { Id = 2, ShipSize = "3", Cargo = "most sausages", ShipName = "Zargon 17.124 epsilon" };
            _vehicleRepo.Add(v3);

            var MrCap = new Captain { Id = 0, Name = "MrCap", Registration = "123", LicenceNo = "321", Vehicles = new List<Vehicle>
                {
                    v1,
                    v2
                }
            };
            _captainRepo.Add(MrCap);
            var Handoo = new Captain { Id = 1, Name = "Handoo", Registration = "654", LicenceNo = "456",
                Vehicles = new List<Vehicle>
                {
                    v3
                }
            };
            _captainRepo.Add(Handoo);

            var d1 = new Dock { Id = 0, DockNumber = 1 };
            _dockRepo.Add(d1);
            var d2 = new Dock { Id = 1, DockNumber = 2 };
            _dockRepo.Add(d2);
            var d3 = new Dock { Id = 0, DockNumber = 1 };
            _dockRepo.Add(d3);

            _BookingRepo.Add(new Booking { Id = 0, Captain = MrCap, Dock = d1, Vehicle = v1, Time = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 16, 0, 0) });
            _BookingRepo.Add(new Booking { Id = 0, Captain = Handoo, Dock = d2, Vehicle = v2, Time = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 18, 0, 0) });


        }
    }
}