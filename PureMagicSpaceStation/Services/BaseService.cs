﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureMagicSpaceStation.Services
{   //The template for all of our services
    public abstract class BaseService<T>
        where T : BaseService<T>, new()
    {
        private static T _instance = new T();

        public static T Instance
        {
            get
            {
                return _instance;
            }
        }
    }
}
