﻿using PureMagicSpaceStation.DAL;
using PureMagicSpaceStation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PureMagicSpaceStation.Services
{
    public class VehicleService : BaseService<VehicleService>
    {
        private Repository<Vehicle> vehicleRepo = Repository<Vehicle>.Instance;
        private Repository<Captain> captainRepo = Repository<Captain>.Instance;
        private AccountService accountService = AccountService.Instance;

        public List<Vehicle> GetCaptainsVehicles(int captainId)
        {
            var captain = captainRepo.Find(c => c.Id == captainId);

            if (captain == null)
                return null;

            return captain.Vehicles;
        }

        public Vehicle GetVehicle(int vehicleId)
        {
            return vehicleRepo.Find(vehicleId);
        }
    }
}
