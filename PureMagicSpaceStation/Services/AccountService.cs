﻿using PureMagicSpaceStation.DAL;
using PureMagicSpaceStation.Models;
using System.Collections.Generic;
using System.Linq;

namespace PureMagicSpaceStation.Services
{
    public class AccountService : BaseService<AccountService>
    {
        private Repository<User> _userRepo = Repository<User>.Instance;

        // Our currently logged in user
        public User CurrentUser = null;

        public User LogIn(string username, string password)
        {
            // Find any user which matches the passed in username
            var user = _userRepo.Find(u => u.UserName == username);

            // User not found or they have been deleted
            if (user == null || user.IsDeleted)
                return null;

            // If we found a user, check the password. If it's a match, log in.
            if (user.Password == password)
            {
                CurrentUser = user;
                return CurrentUser;
            }

            // Otherwise, wrong password
            return null;
        }

        public List<User> GetUsers()
        {
            return _userRepo.GetAll().Where(u => !u.IsDeleted).ToList();
        }

        public void DeleteUser(int userId)
        {
            _userRepo.Delete(userId);
        }

        public bool Create(string UserName, string Password, bool IsAdmin)
        {
            var registeredUser = _userRepo.Find(u => u.UserName == UserName);
            if (registeredUser != null)
            {
                return false;
            }

            var user = new User
            {
                Id = _userRepo.GetAll().Count + 1,
                UserName = UserName,
                Password = Password,
                IsAdmin = IsAdmin
            };

            _userRepo.Add(user);
            _userRepo.Commit();

            return true;
        }

        public void LogOut()
        {
            CurrentUser = null;
        }
    }
}