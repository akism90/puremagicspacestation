﻿using PureMagicSpaceStation.DAL;
using PureMagicSpaceStation.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PureMagicSpaceStation.Services
{
    public class BookingService : BaseService<BookingService>
    {
        private Repository<Booking> _bookingRepo = Repository<Booking>.Instance;
        private AccountService accountService = AccountService.Instance;
        private VehicleService vehicleService = VehicleService.Instance;
        private CaptainService captainService = CaptainService.Instance;


        public List<Booking> GetBookings()
        {
            return _bookingRepo.GetAll().Where(b => !b.IsDeleted).ToList();
        }

        public void DeleteBooking(int Id)
        {
            _bookingRepo.Delete(Id);
        }

        public bool CreateBooking(int captainId, int vehicleId, DateTime bookingTime)
        {
            var captain = captainService.GetCaptains(captainId);
            var vehicle = vehicleService.GetVehicle(vehicleId);

            // TODO: Check if there is already a booking at that time
            // this.CheckTimeSlotIsAvailable(bookingTime); << returns bool

            var booking = new Booking
            {
                Captain = captain,
                Vehicle = vehicle,
                Confirmed = true,
                Time = bookingTime
            };

            _bookingRepo.Add(booking);

            return true;
        }
    }
}