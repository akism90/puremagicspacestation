﻿using PureMagicSpaceStation.DAL;
using PureMagicSpaceStation.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PureMagicSpaceStation.Services
{
    public class CaptainService : BaseService<CaptainService>
    {
        private Repository<Captain> captainRepo = Repository<Captain>.Instance;
        private AccountService accountService = AccountService.Instance;

        public List<Captain> GetCaptains()
        {
            return captainRepo.GetAll()
                .Where(b => !b.IsDeleted)
                .ToList();
        }

        public Captain GetCaptains(int captainId)
        {
            return captainRepo.Find(captainId);
        }
    }
}