﻿using PureMagicSpaceStation.DAL;
using PureMagicSpaceStation.MVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PureMagicSpaceStation
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Seed repository data
            MockData.Seed();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new WelcomeLoginPage());
            Application.Run(new WelcomeLoginPage());
           
        }
    }
}
